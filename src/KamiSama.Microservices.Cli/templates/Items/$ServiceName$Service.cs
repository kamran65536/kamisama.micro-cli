﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KamiSama.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace KamiSama.Microservices.Services
{
	[Inject(lifetime: ServiceLifetime.$Lifetime$)]
	public class $ServiceName$Service : I$ServiceName$Service
	{
		public Task ExecuteAsync()
		{
			throw new NotImplementedException();
		}
	}
}
