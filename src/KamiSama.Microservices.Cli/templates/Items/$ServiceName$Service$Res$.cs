﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KamiSama.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using KamiSama.Microservices.Dto;

namespace KamiSama.Microservices.Services
{
	[Inject(lifetime: ServiceLifetime.$Lifetime$)]
	public class $ServiceName$Service : I$ServiceName$Service
	{
		public Task<$ServiceName$ServiceResponse> ExecuteAsync()
		{
			throw new NotImplementedException();
		}
	}
}
