﻿namespace KamiSama.Microservices
{
	public interface I$ServiceName$Service
	{
		Task ExecuteAsync();
	}
}
