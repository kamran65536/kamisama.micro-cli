﻿using System.Threading.Tasks;
using KamiSama.Microservices.Dto;

namespace KamiSama.Microservices
{
	public interface I$ServiceName$Service
	{
		Task<$ServiceName$ServiceResponse> ExecuteAsync($ServiceName$ServiceRequest request);
	}
}
