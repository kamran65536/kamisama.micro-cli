﻿namespace KamiSama.Microservices.Cli.DataTypes
{
	public enum ProjectType
	{
		Dto,
		Abstractions,
		Services,
		Server
	}
}
