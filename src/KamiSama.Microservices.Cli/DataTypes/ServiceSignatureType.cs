﻿using System;

namespace KamiSama.Microservices.Cli.DataTypes
{
	[Flags]
	public enum ServiceSignatureType
	{
		Req = 1,
		Res = 2,
	}
}
