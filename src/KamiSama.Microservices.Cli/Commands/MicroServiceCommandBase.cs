﻿using CliFx;
using CliFx.Attributes;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Cli.Commands
{
	public abstract class MicroServiceCommandBase : ICommand
	{
		[CommandOption("namespace", 'n', Description = "The microservice namespace and also the project's name's prefix.", IsRequired = true)]
		public string Namespace { get; set; }
		private DirectoryInfo templatesDirectory;

		public DirectoryInfo TemplatesDirectory
		{
			get
			{
				if (templatesDirectory != null)
					return templatesDirectory;

				var executingPath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName;
				return templatesDirectory = new DirectoryInfo(Path.Combine(executingPath, "templates"));
			}
		}

		protected Dictionary<string, string> GetDefaultMap(params (string key, string value)[] additionalValues)
		{
			var dic = new Dictionary<string, string>
			{
				{  "KamiSama.Microservices", Namespace },
				{  "KamiSamaMicroservices", Namespace + "Services" },
				{ "$Namespace$", Namespace },
			};

			foreach (var (key, value) in additionalValues)
				dic[key] = value;

			return dic;
		}
		

		protected void CopyTemplateFile(IConsole console, FileInfo sourceFile, DirectoryInfo destinationDirectory, Dictionary<string, string> valueMap = null)
		{
			valueMap ??= GetDefaultMap();

			var content = File.ReadAllText(sourceFile.FullName);

			foreach (var keyValue in valueMap)
				content = content.Replace(keyValue.Key, keyValue.Value);


			var destinationFileName = Path.Combine(destinationDirectory.FullName, sourceFile.Name);

			foreach (var keyValue in valueMap)
				destinationFileName = destinationFileName.Replace(keyValue.Key, keyValue.Value);

			console.Output.WriteLine($"Creating file: {destinationFileName}...");

			if (!destinationDirectory.Exists)
				destinationDirectory.Create();

			File.WriteAllText(destinationFileName, content);
		}

		protected void DotNet(IConsole console, string arguments, DirectoryInfo workingDirectory)
		{
			console.Output.WriteLine($"dotnet " + arguments);

			var startInfo = new ProcessStartInfo
			{
				FileName = "dotnet.exe",
				WorkingDirectory = workingDirectory.FullName,
				Arguments = arguments
			};
			Process.Start(startInfo).WaitForExit();
		}

		public abstract ValueTask ExecuteAsync(IConsole console);
	}
}
