﻿using CliFx;
using CliFx.Attributes;
using KamiSama.Microservices.Cli.DataTypes;
using System.IO;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Cli.Commands
{
	[Command("project add", Description = "Creates a project of defined microservice type.")]
	public class ProjectAddCommand : MicroServiceCommandBase
	{
		[CommandOption("type", 't', Description = "Project Type.", IsRequired = true)]
		public ProjectType ProjectType { get; set; }
		[CommandOption("output", 'o', Description = "Output dirctory.")]
		public string OutputDirectory { get; set; } = "./$Namespace$.$ProjectType$";

		public override ValueTask ExecuteAsync(IConsole console)
		{
			var valueMap = GetDefaultMap(
				("$ProjectType$", ProjectType + ""),
				("$Namespace$", Namespace));


			var sourcePath = Path.Combine(TemplatesDirectory.FullName, $"KamiSama.Microservices.{ProjectType}");
			var sourceDir = new DirectoryInfo(sourcePath);

			var destinationPath = $"./{Namespace}.{ProjectType}";

			if (!string.IsNullOrWhiteSpace(OutputDirectory))
				destinationPath = OutputDirectory;

			var destinationDir = new DirectoryInfo(destinationPath);

			console.Output.WriteLine($"Adding Project command is called! {Namespace}, {ProjectType}, {destinationDir.FullName}");

			foreach (var file in sourceDir.GetFiles("*", SearchOption.AllDirectories))
			{
				var relativePath = Path.GetRelativePath(sourceDir.FullName, file.FullName);
				var destinationFile = new FileInfo(Path.Combine(destinationDir.FullName, relativePath));

				CopyTemplateFile(console, file, destinationFile.Directory, valueMap);
			}

			DotNet(console, $"sln add {destinationDir.Name}", destinationDir.Parent);

			return default;
		}
	}
}
