﻿using CliFx;
using CliFx.Attributes;
using KamiSama.Microservices.Cli.DataTypes;
using System.IO;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Cli.Commands
{
	[Command("solution add", Description = "Creates new solution and Adds dto, abstractions, services and server project types of microservice projects to the path.")]
	public class SolutionAddCommand : MicroServiceCommandBase
	{
		[CommandOption("output", 'o', Description = "Output path.", IsRequired = false)]
		public string OutputDirectory { get; set; } = ".";

		public override async ValueTask ExecuteAsync(IConsole console)
		{
			var allTypes = new ProjectType[]
			{
				ProjectType.Dto,
				ProjectType.Abstractions,
				ProjectType.Services,
				ProjectType.Server
			};
			DirectoryInfo outDir;

			if (string.IsNullOrWhiteSpace(OutputDirectory))
				outDir = new DirectoryInfo(".");
			else
				outDir = new DirectoryInfo(OutputDirectory);

			DotNet(console, "new sln --name " + Namespace, outDir);

			foreach (var projectType in allTypes)
			{
				var command = new ProjectAddCommand
				{
					Namespace = Namespace,
					OutputDirectory = Path.Combine(outDir.FullName, Namespace + "." + projectType),
					ProjectType = projectType
				};

				await command.ExecuteAsync(console);
			}

			CopyTemplateFile(console, new FileInfo(Path.Combine(TemplatesDirectory.FullName, "Items", ".dockerignore")), outDir);
		}
	}
}
