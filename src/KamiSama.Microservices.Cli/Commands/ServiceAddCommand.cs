﻿using CliFx;
using CliFx.Attributes;
using KamiSama.Microservices.Cli.DataTypes;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Cli.Commands
{
	[Command("service add", Description = "Adds a new service to the environment")]
	public class ServiceAddCommand : MicroServiceCommandBase
	{
		[CommandParameter(0, Name = "ServiceName", Description = "Service name.")]
		public string ServiceName { get; set; }
		[CommandOption("Lifetime", 'l', Description = "Service Lifetime.")]
		public ServiceLifetime Lifetime { get; set; } = ServiceLifetime.Transient;
		[CommandOption("sig", 's', Description = "input and output types")]
		public ServiceSignatureType InOut { get; set; } = ServiceSignatureType.Req | ServiceSignatureType.Res;
		[CommandOption("output", 'o', Description = "Output path.", IsRequired = false)]
		public string OutputDirectory { get; set; } = ".";
		/// <inheritdoc />
		public override ValueTask ExecuteAsync(IConsole console)
		{
			var dtoDir = new DirectoryInfo($"./{Namespace}.Dto");
			var servicesDir = new DirectoryInfo($"./{Namespace}.Services");
			var abstractionsDir = new DirectoryInfo($"./{Namespace}.Abstractions");

			var valueMap = GetDefaultMap();
			var serviceName = ServiceName;

			if (serviceName.ToLower().EndsWith("service"))
				serviceName = serviceName.Substring(serviceName.Length - "Service".Length);

			valueMap["$ServiceName$"] = ServiceName;
			valueMap["$Lifetime$"] = Lifetime + "";
			valueMap["$Req$"] = "";
			valueMap["$Res$"] = "";

			if (InOut.HasFlag(ServiceSignatureType.Req))
			{
				var requestDtoFile = new FileInfo(Path.Combine(TemplatesDirectory.FullName, "Items", "$ServiceName$ServiceRequest.cs"));
				CopyTemplateFile(console, requestDtoFile, dtoDir, valueMap);
			}
			if (InOut.HasFlag(ServiceSignatureType.Res))
				{
				var responseDtoFile = new FileInfo(Path.Combine(TemplatesDirectory.FullName, "Items", "$ServiceName$ServiceResponse.cs"));
				CopyTemplateFile(console, responseDtoFile, dtoDir, valueMap);
			}

			var templateFileName = "$ServiceName$Service";

			if (InOut.HasFlag(ServiceSignatureType.Req))
				templateFileName += "$Req$";
			if (InOut.HasFlag(ServiceSignatureType.Res))
				templateFileName += "$Res$";

			var serviceFile = new FileInfo(Path.Combine(TemplatesDirectory.FullName, "Items", templateFileName) + ".cs");
			var serviceInterfaceFile = new FileInfo(Path.Combine(TemplatesDirectory.FullName, "Items", "I" + templateFileName) + ".cs");

			CopyTemplateFile(console, serviceFile, servicesDir, valueMap);
			CopyTemplateFile(console, serviceInterfaceFile, abstractionsDir, valueMap);

			return default;
		}
	}
}
