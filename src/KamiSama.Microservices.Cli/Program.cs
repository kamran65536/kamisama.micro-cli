﻿using CliFx;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Cli
{
	class Program
	{
		static async Task<int> Main() =>
			await new CliApplicationBuilder()
				.UseDescription("A tool to generate common and tested infrastructrue for microservices.")
				.UseExecutableName("micro")
				.UseTitle("KamiSama microservice tool")
				.AddCommandsFromThisAssembly()
				.Build()
				.RunAsync();
	}
}
