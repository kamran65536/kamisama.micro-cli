@REM @echo off

dotnet build
dotnet pack KamiSama.Microservices.Cli -o nupkgs
dotnet tool uninstall -g KamiSama.Microservices.Cli
dotnet tool install -g KamiSama.Microservices.Cli --add-source ./nupkgs
rd /s/q nupkgs