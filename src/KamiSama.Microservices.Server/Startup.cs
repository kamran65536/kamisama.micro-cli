using Hellang.Middleware.ProblemDetails;
using KamiSama.Extensions;
using KamiSama.Microservices.Server.Utilities;
using KamiSama.Microservices.Services;
using KamiSama.Microservices.Utilties;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Linq;

namespace KamiSama.Microservices.Server
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		/// <summary>
		/// This method gets called by the runtime. Use this method to add services to the container.
		/// </summary>
		/// <param name="services"></param>
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSwaggerDocument(config =>
			{
				config.Title = "KamiSamaMicroservices API";

				config.OperationProcessors.Add(new XTraceHeaderOperationProcessor());
			});
			
			var configs = Configuration.GetSection("exceptions").Get<ExceptionConfig[]>();
			services.AddProblemDetails(options =>
			{
				options.IncludeExceptionDetails = (ctx, exp) => false;
				options.Map<Exception>(exp => MapExceptionFunc(exp, configs));
			});
			services.AddKamiSamaMicroserviceServices(Configuration.GetSection("KamiSama.Microservices"));
			services.AddScoped<XTraceLogScopeAttribute>();
			services.AddControllers();
			services
				.AddHealthChecks()
				.AddCheck<SampleHealthCheck>("sample-health-check");
		}
		private ProblemDetails MapExceptionFunc(Exception exp, ExceptionConfig[] configs)
		{
			var expName = exp.GetType().Name;

			var config = configs.FirstOrDefault(x => x.ExceptionName == exp.GetType().Name);

			if (config == null)
				return new ProblemDetails
				{
					Title = $"**" + exp.Message,
					Status = 500,
				};
			else
				return config.GetDetails(exp);
		}
		/// <summary>
		/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		/// </summary>
		/// <param name="app"></param>
		/// <param name="env"></param>
		public void Configure(IApplicationBuilder app)
		{
			app.UseProblemDetails();
			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapHealthChecks("/health", new HealthCheckOptions
				{
					ResponseWriter = async (context, report) =>
					{
						context.Response.ContentType = "application/json";

						var result = new
						{
							status = report.Status.ToString(),
							errors = report.Entries.Select(e => new { key = e.Key, value = e.Value.Status.ToString() })
						};
						await context.Response.WriteAsync(result.SerializeToJson());
					},
					ResultStatusCodes =
					{
						[HealthStatus.Healthy] = StatusCodes.Status200OK,
						[HealthStatus.Degraded] = StatusCodes.Status200OK,
						[HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
					}
				});
			});


			// Enable the Swagger UI middleware and the Swagger generator
			app.UseOpenApi();
			app.UseSwaggerUi3();
		}
	}
}
