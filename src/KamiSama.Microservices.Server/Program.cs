using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using System.IO;

namespace KamiSama.Microservices.Server
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureAppConfiguration(config =>
				{
					bool exists = File.Exists("./configs/exceptions.json");
					config.AddJsonFile("./configs/exceptions.json", optional: true, reloadOnChange: true);
					config.AddJsonFile("./configs/KamiSama.Microservices-settings.json", optional: true, reloadOnChange: true);
				})
				.ConfigureLogging(config =>
				{
					config.ClearProviders();
					config.AddNLog();
				})
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
