﻿using KamiSama.Microservices;
using KamiSama.Microservices.Dto;
using KamiSama.Microservices.Server.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Radin.IsoService.Server.Controllers
{
	/// <summary>
	/// Sample Controller
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	[ServiceFilter(typeof(XTraceLogScopeAttribute))]
	public class SampleController : ControllerBase
	{
		/// <summary>
		/// Do somethig using sampel service
		/// </summary>
		/// <param name="service"></param>
		/// <param name="request"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("send")]
		public Task<SampleServiceResponse> DoSomethingAsync([FromServices] ISampleService service, [FromBody] SampleServiceRequest request)
			=> service.DoSomething(request);
	}
}
