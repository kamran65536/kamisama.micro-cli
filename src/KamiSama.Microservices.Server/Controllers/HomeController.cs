﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Microservices.Server.Controllers
{
	/// <summary>
	/// Home controller
	/// </summary>
	[Route("/")]
	[OpenApiIgnore]
	[ExcludeFromCodeCoverage]
	public class HomeController : Controller
	{
		/// <summary>
		/// Redirects to swagger
		/// </summary>
		/// <returns></returns>
		public IActionResult Get() => Redirect("~/swagger");
	}
}
