﻿using KamiSama.Extensions;
using KamiSama.Microservices.Utilties;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Microservices.Server.Utilities
{
	/// <summary>
	/// Creates logger scope based on x-username
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class XTraceLogScopeAttribute : ActionFilterAttribute, IDisposable
	{
		/// <summary>
		/// internal logger
		/// </summary>
		private readonly ILogger<XTraceLogScopeAttribute> logger;
		/// <summary>
		/// logger scopde
		/// </summary>
		private IDisposable loggerScope;
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="logger"></param>
		public XTraceLogScopeAttribute(ILogger<XTraceLogScopeAttribute> logger)
		{
			this.logger = logger;
		}
		/// <summary>
		/// disposes the scope
		/// </summary>
		public void Dispose()
		{
			try
			{
				loggerScope?.Dispose();
			}
			catch
			{
			}
		}
		/// <summary>
		/// called on any request markedwith the attribute
		/// </summary>
		/// <param name="context"></param>
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			string xUsername = context.HttpContext.Request.Headers[XTraceHeaderOperationProcessor.TraceHeader] + "";

			if (string.IsNullOrWhiteSpace(xUsername))
				xUsername = Guid.NewGuid().ToByteArray().ToBase64().TrimEnd('=');

			//removing / and \, preventing path hijacking...
			xUsername = xUsername
				.Replace("/", "")
				.Replace(@"\", "");

			loggerScope = logger.BeginScope(new[] { KeyValuePair.Create(XTraceHeaderOperationProcessor.TraceHeader, xUsername) });
		}
	}
}