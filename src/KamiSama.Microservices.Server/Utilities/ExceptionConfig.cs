﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace KamiSama.Microservices.Server.Utilities
{
	public class ExceptionConfig
	{
		public string ExceptionName { get; set; }
		public int MappedStatusCode { get; set; }
		public int MappedErrorCode { get; set; }
		public string Message { get; set; }

		public ProblemDetails GetDetails(Exception exp)
		{
			var details = new ProblemDetails
			{
				Title = Message,
				Status = MappedStatusCode,
				Detail = exp?.Message
			};

			details.Extensions["ErrorCode"] = MappedErrorCode + "";
			details.Extensions["Message"] = Message;

			return details;
		}
	}
}
