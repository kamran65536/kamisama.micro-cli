﻿using NSwag;
using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Microservices.Utilties
{
	/// <summary>
	/// Username processor adds the header to the swagger document generator
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class XTraceHeaderOperationProcessor : IOperationProcessor
	{
		/// <summary>
		/// public x-username header name
		/// </summary>
		public const string TraceHeader = "x-trace";
		/// <summary>
		/// processes the context and adds x-username header to the fields
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public bool Process(OperationProcessorContext context)
		{
			context.OperationDescription.Operation.Parameters.Add(
				new OpenApiParameter
				{
					Name = TraceHeader,
					Kind = OpenApiParameterKind.Header,
					Type = NJsonSchema.JsonObjectType.String,
					IsRequired = false,
					Description = "x-username header for tracing the requests",
				});

			return true;
		}
	}
}
