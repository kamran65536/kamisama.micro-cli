﻿using KamiSama.Microservices.Dto;
using System.Threading.Tasks;

namespace KamiSama.Microservices
{
	public interface ISampleService
	{
		Task<SampleServiceResponse> DoSomething(SampleServiceRequest request);
	}
}
