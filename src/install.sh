#!bin/bash

dotnet build
dotnet pack KamiSama.Microservices.Cli -o nupkgs
dotnet tool uninstal KamiSama.Microservices.Cli
dotnet tool install -g KamiSama.Microservices.Cli --add-source .\KamiSama.Microservices.Cli\bin\Debug\
rm -rf nupkgs