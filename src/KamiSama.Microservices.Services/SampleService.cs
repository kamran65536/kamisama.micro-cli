﻿using KamiSama.Extensions.DependencyInjection;
using KamiSama.Microservices.Dto;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Services
{
	[Inject(lifetime: ServiceLifetime.Scoped)]
	class SampleService : ISampleService
	{
		public Task<SampleServiceResponse> DoSomething(SampleServiceRequest request)
		{
			throw new System.NotImplementedException();
		}
	}
}
