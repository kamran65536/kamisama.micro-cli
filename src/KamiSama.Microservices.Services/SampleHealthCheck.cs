﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Microservices.Services
{
	/// <summary>
	/// Here is a sample health check mechanisim
	/// </summary>
	public class SampleHealthCheck : IHealthCheck
	{
		/// <inheritdoc />
		public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			return Task.FromResult(HealthCheckResult.Healthy());
		}
	}
}
