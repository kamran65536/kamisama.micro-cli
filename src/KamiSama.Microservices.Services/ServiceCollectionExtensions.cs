﻿using KamiSama.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace KamiSama.Microservices.Services
{
	/// <summary>
	/// Service Collection extensions
	/// </summary>
	public static class ServiceCollectionExtensions
	{
		/// <summary>
		/// Adds and configures the KamiSamaMicroservices
		/// </summary>
		/// <param name="services"></param>
		/// <param name="section"></param>
		/// <returns></returns>
		public static IServiceCollection AddKamiSamaMicroserviceServices(this IServiceCollection services, IConfigurationSection section)
		{
			services.AutoAddDependencies(typeof(ServiceCollectionExtensions).Assembly);

			services.Configure<KamiSamaMicroservicesSettings>(section);

			return services;
		}
	}
}
