# Micro CLI

The micro-cli is a command line interface to generate minimal but full feature .net core micro service.
It includes the following feature:
- logging
- containerized (Dockerfile)
- configurable
- traceable
- healthcheck
- exception handling by configuration

# Command Line
## Create new solution
```bash
micro solution add -n SampleMicroServiceName
```
## Create new project
```bash
micro project add -t dto -n SampleMicroServiceName
```
## Add new service
```bash
micro service add NewService --lifetime scoped --sig Req,Res -n SampleMicroServiceName
```

# Install and Uninstall
## Installing
```bash
dotnet tool install -g KamiSama.Microservices.Cli
```
## Uninstalling
```bash
dotnet tool uninstall -g KamiSama.Microservices.Cli
```